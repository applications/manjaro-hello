<big> Dzięki za wsparcie </big>

Manjaro Linux otrzymuje ogromne wsparcie od swojej społeczności użytkowników i chcielibyśmy podziękować każdemu wspierającemu za udział. Rozwijamy się w stałym tempie, a dzięki Tobie nasza dystrybucja z każdym dniem jest coraz lepsza.

Bardzo łatwo jest coś zmienić. W zależności od twojego zestawu umiejętności, twojej dostępności możesz pomóc Manjaro na jeden lub więcej z następujących sposobów:

<big> Wsparcie i promocja </big>

<b> Rozpowszechnianie informacji </b>

Jeśli lubisz Manjaro, daj znać innym. Napisz recenzję i opublikuj ją na distrowatch.com. Porozmawiaj o tym ze swoimi przyjaciółmi i ludźmi wokół ciebie.

<b> Dołączanie do społeczności </b>

Manjaro to nie tylko system operacyjny, ale także dynamiczna społeczność ludzi, którzy cieszą się, gromadzą i wchodzą w interakcje z wolnym i otwartym projektem. Niezależnie od tego, czy chodzi o pomaganie innym w rozwiązywaniu problemów, sprawianie, że czują się mile widziani, czy po prostu spotykając się i rozmawiając z innymi użytkownikami Manjaro, zalecamy dołączenie do społeczności i udział w ulepszaniu Manjaro.

<b> Pomaganie innym </b>

Jeśli masz trochę wolnego czasu i chcesz pomóc innym użytkownikom w rozwiązywaniu problemów technicznych, powinieneś poważnie rozważyć czytanie forów i / lub dołączenie do kanału IRC i pomoc innym użytkownikom Manjaro w rozwiązywaniu problemów, które wiesz, jak naprawić.

<big> Wkład do projektu </big>

<b> Raporty o błędach </b>

Jeśli zauważyłeś coś, co nie działa poprawnie podczas korzystania z Manjaro, daj nam znać. Problem, który odkryłeś, prawdopodobnie wpłynie również na innych. Im szybciej się o tym dowiemy, tym szybciej będziemy mogli to naprawić.

<b> Nowe pomysły </b>

Zdecydowana większość ulepszeń zawartych w każdym wydaniu pochodzi od społeczności. Jeśli Twoim zdaniem brakuje czegoś, co można zrobić lepiej, poinformuj nas o tym. Niezależnie od tego, czy jest to dołączenie brakującego sterownika sprzętu, czy aplikacji, która powinna być częścią standardowej instalacji, czy też masz inne pomysły, jak ulepszyć Manjaro, zawsze jesteśmy zainteresowani ich usłyszeniem.

<b> Grafika </b>

Jeśli masz talent do projektowania graficznego i chcesz wnieść swój wkład w projekt, prześlij nam swoje kreacje i grafiki. Niezależnie od tego, czy jest to prosta tapeta, zestaw ikon, ekran powitalny, czy nawet nowe logo, zawsze jesteśmy zainteresowani usłyszeniem od Ciebie o nowej grafice.

<b> Kod </b>

Większość naszego kodu jest pisana w QT, C ++, Pythonie, HTML5 / CSS i BASH. Używamy również Gita do kontroli wersji i PKGBUILD do pakowania. Jeśli czujesz się komfortowo z tymi technologiami, nie wahaj się rzucić okiem na kod. Jeśli myślisz, że możesz ulepszyć nasze aplikacje lub napisać nowe, nie wahaj się zasugerować łatek lub rozwidlić nasze repozytoria git.
